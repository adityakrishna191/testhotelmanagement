import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class FoodTest {
    Food foodTypeSandwich = new Food(1, 1);

    @BeforeClass
    public static void printBeforeFoodClass(){
        System.out.println("Beginning tests for Food class");
    }

    @Test
    public void testFoodPrice(){
        double expected = 50;
        double actual = foodTypeSandwich.price;
       assertEquals(expected,actual,0.0);
    }

    @Test
    public void testFoodQuantity(){
        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
    }

    @Test
    public void testFoodNotNull(){
        assertNotNull(foodTypeSandwich);
    }

    @Test
    public void testAssertTrue(){
        assertTrue(foodTypeSandwich.itemno == 1);
    }

    @Test
    public void testAssertFalse(){
        assertFalse(foodTypeSandwich.quantity == 2);
    }

    @AfterClass
    public static void printAfterFoodClass(){
        System.out.println("Ending tests for Food class");
    }
}