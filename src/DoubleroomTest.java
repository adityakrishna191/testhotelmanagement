import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleroomTest {
    Doubleroom doubleRoom1 = new Doubleroom("Neo", "4269", "M", "Trinity", "4269", "F");

    @BeforeClass
    public static void printBeforeDoubleRoomTest(){
        System.out.println("Beginning tests for Doubleroom class");
    }

    @Test
    public void testRoomOwnerName(){
        String expected = "Trinity";
        String actual = doubleRoom1.name2;
        assertEquals(expected,actual);
    }

    @Test
    public void testRoomOwnerContact(){
        String expected = "4269";
        String actual = doubleRoom1.contact2;
        assertTrue(actual.equals(expected));
    }

    @Test
    public void testRoomOwnerGender(){
        String expected = "M";
        String actual = doubleRoom1.gender2;
        assertFalse(actual.equals(expected));
    }

    @Test
    public void testAssertNotNull(){
        assertNotNull(doubleRoom1);
    }

    @Test
    public void testAssertNotSame(){
        assertNotSame(doubleRoom1.name, doubleRoom1.name2);
    }

    @Test
    public void testAssertSame(){
        assertSame(doubleRoom1.contact, doubleRoom1.contact2);
    }

    @AfterClass
    public static void printAfterDoubleRoomTest(){
        System.out.println("Ending tests for Doubleroom class");
    }
}