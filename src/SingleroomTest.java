import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class SingleroomTest {
    Singleroom singleRoom1 = new Singleroom("Neo", "4269", "M");
    Singleroom singleRoom2 = new Singleroom("Trinity", "9624", "F");

    @BeforeClass
    public static void printBeforeSingleRoomTest(){
        System.out.println("Beginning tests for Singleroom class");
    }

    @Test
    public void testRoomOwnerName(){
        String expected = "Neo";
        String actual = singleRoom1.name;
        assertEquals(expected,actual);
    }

    @Test
    public void testRoomOwnerContact(){
        String expected = "4269";
        String actual = singleRoom1.contact;
        assertTrue(actual.equals(expected));
    }

    @Test
    public void testRoomOwnerGender(){
        String expected = "F";
        String actual = singleRoom1.gender;
        assertFalse(actual.equals(expected));
    }

    @Test
    public void testAssertNotNull(){
        assertNotNull(singleRoom1);
    }

    @Test
    public void testAssertNotSame(){
        assertNotSame(singleRoom1, singleRoom2);
    }

    @AfterClass
    public static void printAfterSingleRoomTest(){
        System.out.println("Ending tests for Singleroom class");
    }
}